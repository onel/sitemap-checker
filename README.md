sitemap-checker
==========

sitemap-checker its simple script for checking urls in sitemap.xml.


Usage
-------------

```
./sitemap.php /PATH/TO/sitemap.xml
```
Redirect output:
```
./sitemap.php /PATH/TO/sitemap.xml > logfile.txt
```