#!/usr/bin/env php
<?php

if( !isset($argv[1]) ){
    echo "Usage: ./sitemap.php /PATH/TO/sitemap.xml \n";
    echo "Redirect output: ./sitemap.php /PATH/TO/sitemap.xml > logfile.txt \n";
    exit();
}

$sitemapFile = $argv[1];
if(!is_file($sitemapFile)){
    exit("error: file ".$sitemapFile." not found \n");
}


$DOMDocument = new DOMDocument();
$DOMDocument->preserveWhiteSpace = false;
$res = $DOMDocument->load($sitemapFile);
if(!$res){
    exit("error: xml load error \n");
}

$XPath = new DOMXPath($DOMDocument);

// fetch all namespaces
$namespaces = array();
$xpath = new DOMXPath($DOMDocument);
foreach( $XPath->query('namespace::*') as $node ) {
  $namespaces[$node->nodeName]  = $node->nodeValue;
}

// register xmlns if set
if(isset($namespaces['xmlns'])){
    $XPath->registerNamespace('ns', $namespaces['xmlns']);
}else{
    exit("error: xml file without xmlns \n");
}

$urls = array();
foreach($XPath->query('/ns:urlset/ns:url/ns:loc') as $url) {
    $urls[] = $url->nodeValue;
}

echo "Found urls:\n";
print_r($urls);

$handle = curl_init();
foreach($urls as $ur){
    curl_setopt($handle,  CURLOPT_URL, $ur);
    curl_setopt($handle,  CURLOPT_RETURNTRANSFER, TRUE);
    $response = curl_exec($handle);

    $httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);
    if($httpCode == 404 || $httpCode == 500 || $httpCode == 505 || $httpCode == 504 ) {
        echo '*** error('.$httpCode.'): '.$ur."\n";
    }else{
        echo 'success: '.$ur."\n";
    }
}
curl_close($handle);
